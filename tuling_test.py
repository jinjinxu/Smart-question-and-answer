import requests

# 文本（问题）--文本 （答案）  不同的问题，不同的回答  调用图灵机器人的接口
tuling_url = "http://openapi.tuling123.com/openapi/api/v2"


data = {
    "reqType": 0,
    "perception": {
        "inputText": {
            "text": "s%"
        },
    },
    "userInfo": {
        "apiKey": "apiKey",
        "userId": "userId"
    }
}

def to_tuling(Q):
    data["perception"]["inputText"]["text"] = Q
    s = requests.post(tuling_url,json=data)

    res = s.json().get("results")[0].get("values").get("text")

    return res
