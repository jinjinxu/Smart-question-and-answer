
from flask import Flask,request,render_template,jsonify,send_file
from uuid import uuid4
import os
import ssqa

# 模块的名称将会因其作为单独应用启动还是作为模块导入而有不同
# 这样 Flask 才知道到哪去找模板、静态文件等等
app = Flask(__name__)
app.debug = True  #开发调试模式，代码改动自动重启

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/uploader",methods=["POST"])
def uploader():
    # print(request.files)
    file = request.files.get("reco")
    file_name = os.path.join("audio",f"{uuid4()}.wav")
    file.save(file_name)
    ret_filename = ssqa.Q_A(file_name)
    return jsonify({"filename":ret_filename})

@app.route("/get_audio/<filename>")
def get_audio(filename):
    file = os.path.join("audio",filename)
    return send_file(file)

# 确保服务器只会在该脚本被 Python 解释器直接执行的时候才会运行，而不是作为模块导入的时候。
# __name__ == '__main__' 直接执行
# __name__ == 'app.py'   导入时
if __name__ == '__main__':
    app.run()


