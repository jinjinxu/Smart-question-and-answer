from aip import AipNlp
import tuling_test

""" 你的 APPID AK SK """
APP_ID = 'APPID'
API_KEY = 'AK'
SECRET_KEY = 'SK'

client = AipNlp(APP_ID, API_KEY, SECRET_KEY)

def my_nlp(Q):
    return tuling_test.to_tuling(Q)

