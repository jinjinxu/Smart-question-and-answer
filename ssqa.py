from aip import AipSpeech
from uuid import uuid4
import os
import my_nlp

""" 你的 APPID AK SK """
APP_ID = 'APPID'
API_KEY = 'AK'
SECRET_KEY = 'SK'

client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

# 读取文件
def get_file_content(filePath):
    # m4a--pcm
    any2pcm_str=f"ffmpeg -y  -i {filePath}  -acodec pcm_s16le -f s16le -ac 1 -ar 16000 {filePath}.pcm"
    os.system(any2pcm_str)
    with open(f"{filePath}.pcm", 'rb') as fp:
        return fp.read()


def Q_A(file_name):
    res = client.asr(get_file_content(file_name), 'pcm', 16000, { #输入端
        'dev_pid': 1536,
    })

    Q = res.get('result')[0] #语音（问题）--文字（问题）语音识别
    s = my_nlp.my_nlp(Q)    #文字（问题）--文字（答案） 图灵机器人

    # 文字（答案）--音频 （答案）   语音合成
    result  = client.synthesis(s, 'zh', 1, {
        'vol': 5,
    })

    # 识别正确返回语音二进制 错误则返回dict 参照下面错误码
    if not isinstance(result, dict):
        ret_filename = f"{uuid4()}.mp3"
        new_filename = os.path.join("audio",ret_filename)
        with open(new_filename, 'wb') as f:
            f.write(result)

    return ret_filename
    # os.system('auido.mp3') #系统调用播放音频（答案）